/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollBar;

/**
 *
 * @author Administrator
 */
public class ScrollBarExample {
    
    public ScrollBarExample() {
        JFrame frame;
        JScrollBar scrb;
        JLabel lbl;
        
        frame = new JFrame("Scrollbar Example");
        frame.setSize(300, 300);
        
        lbl = new JLabel();
        lbl.setHorizontalAlignment(JLabel.CENTER);
        lbl.setSize(250, 100);
        
        scrb = new JScrollBar();
        scrb.setBounds(100, 100, 50, 100);
        
        frame.add(scrb);
        frame.add(lbl);
        
        frame.setLayout(null);
        frame.setVisible(true);
        
        scrb.addAdjustmentListener(new AdjustmentListener() {
            @Override
            public void adjustmentValueChanged(AdjustmentEvent e) {
                lbl.setText("Vertical Scrollbar value is: " + scrb.getValue());
            }
            
        });
    }
    
    public static void main(String[] args) {
        ScrollBarExample scrbExam = new ScrollBarExample();
    }
}
