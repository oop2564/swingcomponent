/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;

/**
 *
 * @author Administrator
 */
public class DesktopPaneExample extends JFrame {

    public DesktopPaneExample() {
        CustomDesktopPane desktopPane = new CustomDesktopPane();
        Container contentPane = getContentPane();
        contentPane.add(desktopPane, BorderLayout.CENTER);
        desktopPane.display(desktopPane);

        setTitle("JDesktopPane Example");
        setSize(300, 350);
        setVisible(true);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        DesktopPaneExample deskpaneExam = new DesktopPaneExample();
    }
}

class CustomDesktopPane extends JDesktopPane {

    int numFrames = 3, x = 30, y = 30;

    public void display(CustomDesktopPane dp) {
        for (int i = 0; i < numFrames; ++i) {
            JInternalFrame internalfrm = new JInternalFrame("Internal Frame " + i, true, true, true, true);

            internalfrm.setBounds(x, y, 250, 85);
            Container c1 = internalfrm.getContentPane();
            c1.add(new JLabel("I love my country"));
            dp.add(internalfrm);
            internalfrm.setVisible(true);
            y += 85;
        }
    }
}
