/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

/**
 *
 * @author Administrator
 */
public class SeparatorExample {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Separator Example");
        frame.setSize(400, 100);
        frame.setLayout(new GridLayout(0,1));
        
        JLabel lbl1 = new JLabel("Above Separator");
        
        JSeparator sep = new JSeparator();
        
        JLabel lbl2 = new JLabel("Below Separator");
                
        frame.add(lbl1);
        frame.add(sep);
        frame.add(lbl2);
        
        frame.setVisible(true);
    }
}
