/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;

/**
 *
 * @author Administrator
 */
public class ListExample {

    JLabel lbl;
    JButton btn;

    public ListExample() {
        JFrame frame = new JFrame();
        frame.setSize(550, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        lbl = new JLabel();
        lbl.setSize(500, 100);

        btn = new JButton("SHOW");
        btn.setBounds(325, 140, 80, 30);

        final DefaultListModel<String> L1 = new DefaultListModel<>();
        L1.addElement("Fanta");
        L1.addElement("Pepsi");
        L1.addElement("Coca Cola");
        L1.addElement("Schweppes");

        final JList<String> list = new JList<>(L1);
        list.setBounds(125, 75, 135, 75);

        DefaultListModel<String> L2 = new DefaultListModel<>();
        L2.addElement("Plastic bottle");
        L2.addElement("Glass disposable bottle");
        L2.addElement("Glass returnable bottle");
        L2.addElement("Metal cans");

        final JList<String> list1 = new JList<>(L2);
        list1.setBounds(125, 170, 135, 75);

        frame.add(list);
        frame.add(list1);
        frame.add(btn);
        frame.add(lbl);

        frame.setLayout(null);
        frame.setVisible(true);

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String data = "";
                if (list.getSelectedIndex() != -1) {
                    data = "Brand of Soft drink Selected: " + list.getSelectedValue();
                    lbl.setText(data);
                }
                if (list1.getSelectedIndex() != -1) {
                    data += ", Packaging Selected: ";
                    for (Object frame : list1.getSelectedValues()) {
                        data += frame + " ";
                    }
                }
                lbl.setText(data);
            }
        });
    }

    public static void main(String[] args) {
        ListExample listExam = new ListExample();
    }
}
