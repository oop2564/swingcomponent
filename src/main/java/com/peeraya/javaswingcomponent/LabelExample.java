/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

/**
 *
 * @author Administrator
 */
public class LabelExample extends JFrame {
    
    public static void main(String[] args) {
        JFrame frame;        
        JLabel lbl1;
        JTextField txt;
        JButton btn;
        
        frame = new JFrame("Label Example");
        frame.setSize(350, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        lbl1 = new JLabel();
        lbl1.setBounds(50, 100, 300, 20);
        
        txt = new JTextField();
        txt.setBounds(50, 50, 150, 20);
        
        btn = new JButton("Find IP");
        btn.setBounds(50, 150, 95, 30);
        btn.addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String host = txt.getText();
                    String ip = java.net.InetAddress.getByName(host).getHostAddress();
                    lbl1.setText("IP of " + host + " is " + ip);
                } catch(Exception ex) {
                    System.out.println(ex);
                }
            }
            
        });
                
        frame.add(lbl1);
        frame.add(txt);
        frame.add(btn);
        
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
