/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author Administrator
 */
public class ButtonExample {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Button Example");
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        final JTextField txt = new JTextField();
        txt.setBounds(65, 50, 260, 20);
        
        JButton btn = new JButton(new ImageIcon("D:\\iconnn.png"));
        btn.setBounds(145, 100, 95, 30);
        btn.addActionListener(new ActionListener() {           
            @Override
            public void actionPerformed(ActionEvent e) {
                txt.setText("Welcome to Java Swing Component Project!!");
            }
        });
        
        frame.add(txt);
        frame.add(btn);
        
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
