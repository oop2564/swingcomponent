/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

/**
 *
 * @author Administrator
 */
public class TextAreaExample implements ActionListener {

    JTextArea txtA;
    JLabel lbl1, lbl2;
    JButton btn;

    public TextAreaExample() {
        JFrame frame;

        frame = new JFrame("TextArea Example");
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        lbl1 = new JLabel("Words: xxx");
        lbl1.setBounds(90, 20, 100, 30);
        
        lbl2 = new JLabel("Characters: xxx");
        lbl2.setBounds(205, 20, 100, 30);
        
        txtA = new JTextArea();
        txtA.setBounds(20, 50, 345, 150);
        
        btn = new JButton("Count Words");
        btn.setBounds(135, 215, 120, 30);
        btn.addActionListener(this);

        frame.add(lbl1);
        frame.add(lbl2);
        frame.add(txtA);
        frame.add(btn);

        frame.setLayout(null);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String text = txtA.getText();
        String word[] = text.split("\\s");
        lbl1.setText("Words: " + word.length);
        lbl2.setText("Characters: " + text.length());
    }

    public static void main(String[] args) {
        TextAreaExample txtAexam = new TextAreaExample();
    }
}
