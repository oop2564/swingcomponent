/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JProgressBar;

/**
 *
 * @author Administrator
 */
public class ProgressBarExample extends JFrame {

    JProgressBar pgb;
    int i = 0, num = 0;

    public ProgressBarExample() {
        pgb = new JProgressBar(0, 2000);
        pgb.setBounds(40, 40, 160, 30);
        pgb.setValue(0);
        pgb.setStringPainted(true);

        add(pgb);
        setSize(250, 150);
        setLayout(null);
    }

    public void iterate() {
        while (i <= 2000) {
            pgb.setValue(i);
            i = i + 20;
            try {
                Thread.sleep(150);
            } catch (Exception e) {
                System.out.println("Error!!");
            }
        }
    }

    public static void main(String[] args) {
        ProgressBarExample progbExam = new ProgressBarExample();
        progbExam.setVisible(true);
        progbExam.iterate();
    }
}
