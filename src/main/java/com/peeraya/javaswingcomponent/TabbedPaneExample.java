/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

/**
 *
 * @author Administrator
 */
public class TabbedPaneExample {

    JFrame frame;

    TabbedPaneExample() {
        frame = new JFrame();
        frame.setSize(350, 350);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JTextArea txtA = new JTextArea(200, 200);

        JPanel p1 = new JPanel();
        p1.add(txtA);
        JPanel p2 = new JPanel();
        JPanel p3 = new JPanel();

        JTabbedPane tbp = new JTabbedPane();
        tbp.setBounds(50, 50, 200, 200);

        tbp.add("main", p1);
        tbp.add("visit", p2);
        tbp.add("help", p3);

        frame.add(tbp);

        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        TabbedPaneExample tabpaneExam = new TabbedPaneExample();
    }
}
