/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author Administrator
 */
public class TextFieldExample implements ActionListener {

    JTextField txt1, txt2, txt3;
    JButton btn1, btn2, btn3, btn4;

    public TextFieldExample() {

        JFrame frame = new JFrame();
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        txt1 = new JTextField();
        txt1.setBounds(120, 25, 150, 20);

        txt2 = new JTextField();
        txt2.setBounds(120, 75, 150, 20);

        txt3 = new JTextField(); // สร้างช่อง JTextField
        txt3.setBounds(120, 125, 150, 20);
        txt3.setEditable(false); // ลบขอบกรอบช่อง JTextField

        btn1 = new JButton("+");
        btn1.setBounds(50, 175, 50, 50);
        btn1.addActionListener(this);

        btn2 = new JButton("-");
        btn2.setBounds(130, 175, 50, 50);
        btn2.addActionListener(this);

        btn3 = new JButton("*");
        btn3.setBounds(210, 175, 50, 50);
        btn3.addActionListener(this);

        btn4 = new JButton("/");
        btn4.setBounds(290, 175, 50, 50);
        btn4.addActionListener(this);

        frame.add(txt1);
        frame.add(txt2);
        frame.add(txt3);
        frame.add(btn1);
        frame.add(btn2);
        frame.add(btn3);
        frame.add(btn4);

        frame.setLayout(null);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String str1 = txt1.getText();
        String str2 = txt2.getText();
        double a = Double.parseDouble(str1);
        double b = Double.parseDouble(str2);
        double c = 0.0;
        if (e.getSource() == btn1) {
            c = a + b;
        } else if (e.getSource() == btn2) {
            c = a - b;
        } else if (e.getSource() == btn3) {
            c = a * b;
        } else if (e.getSource() == btn4) {
            c = a / b;
        }        
        txt3.setText(String.format("%.2f", c));
    }

    public static void main(String[] args) {
        TextFieldExample frame = new TextFieldExample();
    }
}
