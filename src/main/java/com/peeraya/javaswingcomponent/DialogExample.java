/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Administrator
 */
public class DialogExample {

    private static JDialog dialog;

    DialogExample() {
        JFrame frame = new JFrame();

        dialog = new JDialog(frame, "Dialog Example", true);
        dialog.setLayout(new FlowLayout());

        JButton btn = new JButton("OK");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DialogExample.dialog.setVisible(false);
            }
        });

        dialog.add(new JLabel("Click button to continue."));
        dialog.add(btn);
        dialog.setSize(300, 300);
        dialog.setVisible(true);
    }

    public static void main(String args[]) {
        DialogExample diaExam = new DialogExample();
    }
}
