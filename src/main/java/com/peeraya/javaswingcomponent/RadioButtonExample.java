/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

/**
 *
 * @author Administrator
 */
public class RadioButtonExample extends JFrame implements ActionListener {

    JFrame frame;
    JRadioButton rabtn1, rabtn2;
    JButton btn;

    public RadioButtonExample() {

        frame = new JFrame();
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        rabtn1 = new JRadioButton("Male");
        rabtn1.setBounds(100, 50, 100, 30);

        rabtn2 = new JRadioButton("Female");
        rabtn2.setBounds(100, 100, 100, 30);

        ButtonGroup bg = new ButtonGroup(); // รวมปุ่มให้เป็นกลุ่มเพื่อให้เลือกได้ตัวเลือกเดียว
        bg.add(rabtn1);
        bg.add(rabtn2);

        btn = new JButton("Click");
        btn.setBounds(100, 150, 80, 30);
        btn.addActionListener(this);

        frame.add(rabtn1);
        frame.add(rabtn2);
        frame.add(btn);

        frame.setLayout(null);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (rabtn1.isSelected()) {
            JOptionPane.showMessageDialog(this, "You are Male.");
        }
        if (rabtn2.isSelected()) {
            JOptionPane.showMessageDialog(this, "You are Female.");
        }
    }

    public static void main(String[] args) {
        RadioButtonExample rabtnExam = new RadioButtonExample();
    }
}
