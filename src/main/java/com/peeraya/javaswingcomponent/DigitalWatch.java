/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author Administrator
 */
public class DigitalWatch implements Runnable {

    JFrame frame;
    Thread th = null;
    int hrs = 0, min = 0, sec = 0;
    String timeStr = "";
    JButton btn;

    public DigitalWatch() {
        frame = new JFrame();
        frame.setSize(300, 400);

        th = new Thread(this);
        th.start();

        btn = new JButton();
        btn.setBounds(100, 100, 100, 50);

        frame.add(btn);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    @Override
    public void run() {
        try {
            while (true) {
                Calendar calen = Calendar.getInstance();
                hrs = calen.get(Calendar.HOUR_OF_DAY);
                if (hrs > 12) {
                    hrs -= 12;
                }
                min = calen.get(Calendar.MINUTE);
                sec = calen.get(Calendar.SECOND);

                SimpleDateFormat formatter = new SimpleDateFormat("hh:mm:ss");
                Date date = calen.getTime();
                timeStr = formatter.format(date);

                printTime();

                th.sleep(1000);  // interval given in milliseconds  
            }
        } catch (Exception e) {
        }
    }

    public void printTime() {
        btn.setText(timeStr);
    }

    public static void main(String[] args) {
        new DigitalWatch();
    }
}
