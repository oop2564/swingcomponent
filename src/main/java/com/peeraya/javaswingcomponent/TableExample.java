/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

/**
 *
 * @author Administrator
 */
public class TableExample {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Table Example");
        frame.setSize(300, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        String data[][] = {{"100058", "Chanya", "Female", "34"},
            {"160211", "Yong", "Female", "48"},
            {"257327", "Chang", "Male", "24"}};
        String column[] = {"ID", "NAME", "GENDER", "AGE"};

        final JTable tb = new JTable(data, column);
        tb.setCellSelectionEnabled(true);
        ListSelectionModel select = tb.getSelectionModel();
        select.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        select.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                String Data = null;
                int[] row = tb.getSelectedRows();
                int[] columns = tb.getSelectedColumns();
                for (int i = 0; i < row.length; i++) {
                    for (int j = 0; j < columns.length; j++) {
                        Data = (String) tb.getValueAt(row[i], columns[j]);
                    }
                }
                System.out.println("Table element selected is: " + Data);
            }
        });
        JScrollPane scp = new JScrollPane(tb);
        frame.add(scp);
        frame.setVisible(true);
    }
}
