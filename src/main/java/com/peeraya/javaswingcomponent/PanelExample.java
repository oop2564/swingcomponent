/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Administrator
 */
public class PanelExample {

    public PanelExample() {
        JFrame frame = new JFrame("Panel Example");
        frame.setSize(320, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel panel = new JPanel();
        panel.setBounds(40, 80, 200, 200);
        panel.setBackground(Color.BLUE);

        JButton b1 = new JButton("Button 1");
        b1.setBounds(50, 100, 80, 30);
        b1.setBackground(Color.yellow);

        JButton b2 = new JButton("Button 2");
        b2.setBounds(100, 100, 80, 30);
        b2.setBackground(Color.green);

        panel.add(b1);
        panel.add(b2);

        frame.add(panel);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String args[]) {
        PanelExample panelExam = new PanelExample();
    }
}
