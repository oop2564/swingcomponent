/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Administrator
 */
public class ComboBoxExample {

    JFrame frame;
    JLabel lbl;
    JButton btn;
    JComboBox cbb;

    public ComboBoxExample() {
        frame = new JFrame("ComboBox Example");
        frame.setSize(340, 315);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        lbl = new JLabel();
        lbl.setHorizontalAlignment(JLabel.CENTER);
        lbl.setSize(300, 100);

        btn = new JButton("SHOW");
        btn.setBounds(200, 100, 75, 20);

        String beverages[] = {"Lemonade", "Orange juice", "Coffee", "Tea", "Iced Chocolate",
            "Apple Soda", "Soft drinks", "Cocoa", "Milk"};
        cbb = new JComboBox(beverages);
        cbb.setBounds(50, 100, 120, 20);

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                lbl.setText("Drinks&Beverages Selected: " + cbb.getItemAt(cbb.getSelectedIndex()));                
            }
        });

        frame.add(lbl);
        frame.add(btn);
        frame.add(cbb);

        frame.setLayout(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        ComboBoxExample cbbExam = new ComboBoxExample();
    }
}
