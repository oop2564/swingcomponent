/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.BorderLayout;
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JToolBar;

/**
 *
 * @author Administrator
 */
public class ToolBarExample {

    public static void main(final String args[]) {
        JFrame frame = new JFrame("JToolBar Example");
        frame.setSize(450, 250);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JToolBar toolbar = new JToolBar();
        toolbar.setRollover(true);
        
        JButton button = new JButton("File");
        toolbar.add(button);
        toolbar.addSeparator();
        
        toolbar.add(new JButton("Edit"));
        
        toolbar.add(new JComboBox(new String[]{"Opt-1", "Opt-2", "Opt-3", "Opt-4"}));
        Container contentPane = frame.getContentPane();
        contentPane.add(toolbar, BorderLayout.NORTH);
        
        JTextArea txtA = new JTextArea();
        
        JScrollPane mypane = new JScrollPane(txtA);
        contentPane.add(mypane, BorderLayout.EAST);
        
        frame.setVisible(true);
    }
}
