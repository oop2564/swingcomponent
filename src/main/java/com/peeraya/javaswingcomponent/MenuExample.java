/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;

/**
 *
 * @author Administrator
 */
public class MenuExample implements ActionListener {

    JFrame frame;
    JMenuBar mb;
    JMenu file, edit, help;
    JMenuItem cut, copy, paste, selectAll;
    JTextArea txtA;

    public MenuExample() {
        frame = new JFrame();
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        cut = new JMenuItem("cut");
        copy = new JMenuItem("copy");
        paste = new JMenuItem("paste");
        selectAll = new JMenuItem("selectAll");
        //Add ActionListener cut, copy, paste ,selectAll
        cut.addActionListener(this);
        copy.addActionListener(this);
        paste.addActionListener(this);
        selectAll.addActionListener(this);

        mb = new JMenuBar();
        file = new JMenu("File");
        edit = new JMenu("Edit");
        help = new JMenu("Help");

        edit.add(cut); // เพิ่ม cut function ลงใน edit menu
        edit.add(copy); // เพิ่ม copy function ลงใน edit menu
        edit.add(paste); // เพิ่ม paste function ลงใน edit menu
        edit.add(selectAll); // เพิ่ม selectAll function ลงใน edit menu

        mb.add(file); // เพิ่ม file menu ลงใน menu bar ด้านบนขอบจอ
        mb.add(edit); // เพิ่ม edit menu ลงใน menu bar ด้านบนขอบจอ
        mb.add(help); // เพิ่ม help menu ลงใน menu bar ด้านบนขอบจอ

        txtA = new JTextArea();
        txtA.setBounds(5, 5, 360, 320);

        frame.add(mb); // เพิ่ม menu bar ลงบน frame *เพิ่มแค่ menubar เพราะ ทุกอย่างอยู่ใน mb หมดแล้ว
        frame.add(txtA);

        frame.setJMenuBar(mb);
        frame.setLayout(null);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cut) {
            txtA.cut();
        }
        if (e.getSource() == paste) {
            txtA.paste();
        }
        if (e.getSource() == copy) {
            txtA.copy();
        }
        if (e.getSource() == selectAll) {
            txtA.selectAll();
        }
    }

    public static void main(String[] args) {
        MenuExample menuExam = new MenuExample();
    }
}
