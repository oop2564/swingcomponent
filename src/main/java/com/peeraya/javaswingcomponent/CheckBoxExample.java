/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author Administrator
 */
public class CheckBoxExample extends JFrame implements ActionListener {

    JLabel lbl;
    JCheckBox chkB1, chkB2, chkB3, chkB4;
    JButton btn;

    public CheckBoxExample() {

        this.setSize(325, 420);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        lbl = new JLabel("Pet Ordering System");
        lbl.setBounds(50, 50, 300, 20);

        chkB1 = new JCheckBox("Dog 1800 Bath");
        chkB1.setBounds(100, 100, 150, 20);

        chkB2 = new JCheckBox("Cat 2500 Bath");
        chkB2.setBounds(100, 150, 150, 20);

        chkB3 = new JCheckBox("Rabbit 725 Bath");
        chkB3.setBounds(100, 200, 150, 20);
        
        chkB4 = new JCheckBox("Fish 327 Bath");
        chkB4.setBounds(100, 250, 150, 20);

        btn = new JButton("Order");
        btn.setBounds(100, 300, 80, 30);
        btn.addActionListener(this);

        this.add(lbl);
        this.add(chkB1);
        this.add(chkB2);
        this.add(chkB3);
        this.add(chkB4);
        this.add(btn);

        this.setLayout(null);
        this.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        float amount = 0;
        String msg = "";
        if (chkB1.isSelected()) {
            amount += 1800;
            msg = "Dog: 1800\n";
        }
        if (chkB2.isSelected()) {
            amount += 2500;
            msg += "Cat: 2500\n";
        }
        if (chkB3.isSelected()) {
            amount += 725;
            msg += "Rabbit: 725\n";
        }
        if (chkB4.isSelected()) {
            amount += 327;
            msg += "Fish: 327\n";
        }
        msg += "-----------------\n";
        JOptionPane.showMessageDialog(this, msg + "Total: " + amount);
    }

    public static void main(String[] args) {
        CheckBoxExample checkBOXexam = new CheckBoxExample();
    }
}