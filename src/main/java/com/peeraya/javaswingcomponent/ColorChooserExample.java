/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 *
 * @author Administrator
 */
public class ColorChooserExample extends JFrame implements ActionListener {

    JButton btn;
    JFrame frame;
    JTextArea txtA;

    public ColorChooserExample() {
        frame = new JFrame("Color chooser Example");
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        btn = new JButton("Pad Color");
        btn.setBounds(200, 250, 100, 30);
        btn.addActionListener(this);
        
        txtA = new JTextArea();
        txtA.setBounds(10, 10, 300, 200);
        
        frame.add(btn);
        frame.add(txtA);
        
        frame.setLayout(null);
        frame.setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        Color color = JColorChooser.showDialog(this, "Choose", Color.BLUE);
        txtA.setBackground(color);
    }

    public static void main(String[] args) {
        ColorChooserExample colorcExam = new ColorChooserExample();
    }
}
