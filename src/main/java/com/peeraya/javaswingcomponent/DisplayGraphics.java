/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JFrame;

/**
 *
 * @author Administrator
 */
public class DisplayGraphics extends Canvas {

    @Override
    public void paint(Graphics g) {
        g.drawString("HELLO", 40, 40);  // วาดคำว่า HELLO
        setBackground(Color.WHITE);
        g.fillRect(130, 30, 100, 80); // วาดสี่เหลี่ยม rectangle
        g.drawOval(30, 130, 50, 60); // เส้นวงกลม ลงสีขาว
        
        setForeground(Color.BLUE);
        g.fillOval(130, 130, 50, 60); // วงกลม ลงสีน้ำเงิน
        g.drawArc(30, 200, 40, 50, 90, 60); // วาดเส้นรอบวงกลม ไม่ครบวง กำหนดจุดเริ่ม จุดสุดท้าย
        g.fillArc(30, 130, 40, 50, 180, 40); // วาดเส้นรอบวงกลม ไม่ครบวง กำหนดจุดเริ่ม จุดสุดท้าย + ลงสีเป็นชิ้นพิซซ่า
    }

    public static void main(String[] args) {
        DisplayGraphics m = new DisplayGraphics();
        
        JFrame frame = new JFrame();
        frame.add(m);
        frame.setSize(400, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }
}
