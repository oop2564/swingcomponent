/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import javax.swing.JButton;
import javax.swing.JFrame;

/**
 *
 * @author Administrator
 */
public class SwingIntroduction {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setSize(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JButton btn = new JButton("click"); // สร้างปุ่ม
        btn.setBounds(90, 60, 100, 40);

        frame.setLayout(null);
        frame.add(btn); // เพิ่มปุ่ม btn ลงใน JFrame

        frame.setVisible(true);
    }

}
