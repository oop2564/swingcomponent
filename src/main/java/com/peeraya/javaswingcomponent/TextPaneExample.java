/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

/**
 *
 * @author Administrator
 */
public class TextPaneExample {

    public static void main(String args[]) throws BadLocationException {
        JFrame frame = new JFrame("JTextPane Example");
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        Container cp = frame.getContentPane();
        JTextPane txtpane = new JTextPane();
        SimpleAttributeSet attributeSet = new SimpleAttributeSet();
        StyleConstants.setBold(attributeSet, true);

        // Set the attributes before adding text  
        txtpane.setCharacterAttributes(attributeSet, true);
        txtpane.setText("Welcome ");

        attributeSet = new SimpleAttributeSet();
        StyleConstants.setItalic(attributeSet, true);
        StyleConstants.setForeground(attributeSet, Color.WHITE);
        StyleConstants.setBackground(attributeSet, Color.BLUE);

        Document doc = txtpane.getStyledDocument();
        doc.insertString(doc.getLength(), "To Java", attributeSet);

        attributeSet = new SimpleAttributeSet();
        doc.insertString(doc.getLength(), " World", attributeSet);

        JScrollPane scrollPane = new JScrollPane(txtpane);
        cp.add(scrollPane, BorderLayout.CENTER);

        frame.setVisible(true);
    }
}
