/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;

/**
 *
 * @author Administrator
 */
public class ToolTipExample {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Password Field Example");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JPasswordField pass = new JPasswordField();
        pass.setBounds(100, 100, 100, 30);
        pass.setToolTipText("Enter Your Password");
        
        JLabel lbl = new JLabel("Password: ");
        lbl.setBounds(20, 100, 80, 30);
        
        frame.add(pass);
        frame.add(lbl);
        
        frame.setLayout(null);
        frame.setVisible(true);
    }
}
