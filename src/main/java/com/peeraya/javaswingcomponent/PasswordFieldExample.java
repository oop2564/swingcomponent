/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

/**
 *
 * @author Administrator
 */
public class PasswordFieldExample {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Password Field Example");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        final JPasswordField pass = new JPasswordField();
        pass.setBounds(100, 75, 100, 30);

        final JLabel lbl = new JLabel();
        lbl.setBounds(20, 150, 200, 50);

        JLabel lbl1 = new JLabel("Username: ");
        lbl1.setBounds(20, 20, 80, 30);

        JLabel lbl2 = new JLabel("Password: ");
        lbl2.setBounds(20, 75, 80, 30);

        JButton btn = new JButton("Login");
        btn.setBounds(100, 120, 80, 30);

        final JTextField txt = new JTextField();
        txt.setBounds(100, 20, 100, 30);

        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                lbl.setText("Username: " + txt.getText() + ", Password: " + new String(pass.getPassword()));
            }
        });

        frame.add(pass);
        frame.add(lbl);
        frame.add(lbl1);
        frame.add(lbl2);
        frame.add(btn);
        frame.add(txt);

        frame.setLayout(null);
        frame.setVisible(true);
    }
}
