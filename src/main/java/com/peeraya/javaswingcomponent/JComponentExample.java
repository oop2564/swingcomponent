/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peeraya.javaswingcomponent;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JComponent;
import javax.swing.JFrame;

class JCompo extends JComponent {

    @Override
    public void paint(Graphics g) {
        g.setColor(Color.BLUE);
        g.fillRect(30, 30, 100, 100);
    }
}

/**
 *
 * @author Administrator
 */
public class JComponentExample {

    public static void main(String[] arguments) {
        JCompo compo = new JCompo();
        // create a basic JFrame  
        JFrame.setDefaultLookAndFeelDecorated(true);
        
        JFrame frame = new JFrame("JComponent Example");
        frame.setSize(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // add the JComponent to main frame  
        frame.add(compo);
        frame.setVisible(true);
    }
}
